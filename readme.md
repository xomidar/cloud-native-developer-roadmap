# Cloud Native Developer Roadmap 2020

> Roadmap to becoming a [Cloud Native Developer](https://) developer in 2020:

Below you can find a chart demonstrating the paths that you can take and the libraries that you would want to learn to become a Cloud Native Developer. I made this chart as a tip for everyone who asks me, "What should I learn next a Java developer?"

## Disclaimer

> The purpose of this roadmap is to give you an idea about the landscape. The road map will guide you if you are confused about what to learn next, rather than encouraging you to pick what is hip and trendy. You should grow some understanding of why one tool would be better suited for some cases than the other and remember hip and trendy does not always mean best suited for the job

## Roadmap - Version 0.2

![Cloud Native Developer Roadmap - Version 0.2](./images/CloudNativeDeveloperRoadmap-2020v0.2.png)

## Resources
1. Microservices
2. Continuous Delivery
3. Containers
   
## Wrap Up

If you think the roadmap can be improved, please do open a PR with any updates and submit any issues. Also, I will continue to improve this, so you might want to star this repository to revisit.

Idea from : [Golang Developer Roadmap](https://github.com/Alikhll/golang-developer-roadmap)

## Contribution

The roadmap is built using [Draw.io](https://www.draw.io/). Project file can be found at `resources/CloudNativeDeveloperRoadmap-2020.xml` file. To modify it, open draw.io, click **Open Existing Diagram** and choose `xml` file with project. It will open the roadmap for you. Update it, upload and update the images in readme and create a PR (export as png with 300% zoom and minify that with [Compressor.io](https://compressor.io/compress)).

- Open a pull request with improvements
- Discuss ideas in issues
- Spread the word

## License

[![License: CC BY-NC-SA 4.0](https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg)](https://creativecommons.org/licenses/by-nc-sa/4.0/)
